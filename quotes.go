package package_d

import(
	"fmt"
	"math/rand"
)

func SayQuote() {
	quotes := []string{
		"Those who dare to fail miserably can achieve greatly.",
		"I’m a success today because I had a friend who believed in me and I didn’t have the heart to let him down.",
		"Your time is limited, so don’t waste it living someone else’s life. Don’t be trapped by dogma – which is living with the results of other people’s thinking.",
	}
	index := rand.Intn(len(quotes))
	fmt.Println("Hey,", quotes[index])
}
